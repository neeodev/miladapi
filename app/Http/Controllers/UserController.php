<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Laravel\Lumen\Http\ResponseFactory as HttpResponseFactory;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){
        $users = User::get();

        return response()->json($users,200);
    }

    public function show($id){
        $user = User::find($id);

        return response()->json($user,200);
    }

    public function create(Request $request){
        $nom = $request->input('nom');
        $prenom = $request->input('prenom');
        $pseudo = $request->input('pseudo');

        $user = User::create([
            'Nom' => $nom,
            'Prenom' => $prenom,
            'Pseudo' => $pseudo
        ]);

        return response()->json($user, 201);
    }

    public function update(Request $request, $id){
        $user = User::find($id);

        $nom = $request->input('nom');
        $prenom = $request->input('prenom');
        $pseudo = $request->input('pseudo');

        $user->update([
            'Nom' => $nom,
            'Prenom' => $prenom,
            'Pseudo' => $pseudo
        ]);

        return response()->json($user, 200);
    }

    public function destroy($id){
        User::destroy($id);

        return response()->json('Ressource deleted', 200);
    }
    //
}
